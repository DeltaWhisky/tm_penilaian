<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('css/fa/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('css/fa-5/web-fonts-with-css/css/fontawesome-all.css')}}">
<link rel="stylesheet" href="{{url('css/reset.css')}}">
<link rel="stylesheet" href="{{url('css/font.css')}}">
<link rel="stylesheet" href="{{url('css/style.css')}}">
<link rel="stylesheet" href="{{url('css/responsive.css')}}">
