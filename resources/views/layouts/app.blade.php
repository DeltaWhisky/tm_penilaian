<!DOCTYPE html>
<html lang="en">

<head>
	<title>
	@yield('title')
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	@include('layouts.css')
	@yield('css')

</head>

<body>
	<!-- HEADER -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header">
				<span class="logo-header">
					<img src="{{('img/logo-header.png')}}">
				</span>
				@yield('dropdown-right')
			</div>
		</div>
		<!-- MAIN-KONTEN -->
		@yield('content')


	</div>
	<!-- Script -->
	@include('layouts.js')
	@yield('script')
</body>

</html>
