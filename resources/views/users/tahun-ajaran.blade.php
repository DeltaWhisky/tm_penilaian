@extends('layouts.app')
@section('css')
<style type="text/css">
  body {
    margin: 0px;
    padding: 0px;
  }
</style>
@endsection
@section('title', 'Index')
@section('dropdown-right')
@include('layouts.dropdown-right')
@endsection
@section('content')
<!-- bredcrum -->
<div class="row">
  <div class="col-12" style="padding: 0;">
    <div style="width: 100%;">
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="index.php">Home</a></li>
        <li class="active"><span>Tahun Ajaran</span></li>
      </ol>
    </div>
  </div>
</div>
<!-- MAIN KONTEN -->
<div class="row main-konten">
  <div class='col-md-12 flex-inline'>

    <div class='col-md-4'>
      <a href="mapel.php?id_tahun_ajaran=">
        <div class="thumb">
          <header style="text-decoration: none;"> <i class="fa fa-calendar" aria-hidden="true"></i>
            <h3 class="thumb-judul" style="text-decoration: none;">Tahun Ajaran</h3> </header>
          <article>
            <p class="thumb-text"></p>
          </article>
        </div>
      </a>
    </div>
  </div>
</div>
</div>
<div class="clear"></div>
<div class="footer">
  Copyright&copy; SMK Tunas Media. Created by Danang Widianto
</div>
@endsection
