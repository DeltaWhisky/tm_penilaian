<table class="table table-responsive" id="logins-table">
    <thead>
        <tr>
            <th>Password</th>
        <th>Level</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($logins as $login)
        <tr>
            <td>{!! $login->password !!}</td>
            <td>{!! $login->level !!}</td>
            <td>
                {!! Form::open(['route' => ['logins.destroy', $login->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('logins.show', [$login->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('logins.edit', [$login->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>