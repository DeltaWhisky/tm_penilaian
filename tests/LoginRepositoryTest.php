<?php

use App\Models\Login;
use App\Repositories\LoginRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginRepositoryTest extends TestCase
{
    use MakeLoginTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var LoginRepository
     */
    protected $loginRepo;

    public function setUp()
    {
        parent::setUp();
        $this->loginRepo = App::make(LoginRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateLogin()
    {
        $login = $this->fakeLoginData();
        $createdLogin = $this->loginRepo->create($login);
        $createdLogin = $createdLogin->toArray();
        $this->assertArrayHasKey('id', $createdLogin);
        $this->assertNotNull($createdLogin['id'], 'Created Login must have id specified');
        $this->assertNotNull(Login::find($createdLogin['id']), 'Login with given id must be in DB');
        $this->assertModelData($login, $createdLogin);
    }

    /**
     * @test read
     */
    public function testReadLogin()
    {
        $login = $this->makeLogin();
        $dbLogin = $this->loginRepo->find($login->id);
        $dbLogin = $dbLogin->toArray();
        $this->assertModelData($login->toArray(), $dbLogin);
    }

    /**
     * @test update
     */
    public function testUpdateLogin()
    {
        $login = $this->makeLogin();
        $fakeLogin = $this->fakeLoginData();
        $updatedLogin = $this->loginRepo->update($fakeLogin, $login->id);
        $this->assertModelData($fakeLogin, $updatedLogin->toArray());
        $dbLogin = $this->loginRepo->find($login->id);
        $this->assertModelData($fakeLogin, $dbLogin->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteLogin()
    {
        $login = $this->makeLogin();
        $resp = $this->loginRepo->delete($login->id);
        $this->assertTrue($resp);
        $this->assertNull(Login::find($login->id), 'Login should not exist in DB');
    }
}
