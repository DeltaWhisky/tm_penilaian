<?php

namespace App\Repositories;

use App\Models\Login;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LoginRepository
 * @package App\Repositories
 * @version August 12, 2018, 8:24 pm UTC
 *
 * @method Login findWithoutFail($id, $columns = ['*'])
 * @method Login find($id, $columns = ['*'])
 * @method Login first($columns = ['*'])
*/
class LoginRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'password',
        'level'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Login::class;
    }
}
