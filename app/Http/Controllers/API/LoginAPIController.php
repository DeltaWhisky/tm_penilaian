<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLoginAPIRequest;
use App\Http\Requests\API\UpdateLoginAPIRequest;
use App\Models\Login;
use App\Repositories\LoginRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class LoginController
 * @package App\Http\Controllers\API
 */

class LoginAPIController extends AppBaseController
{
    /** @var  LoginRepository */
    private $loginRepository;

    public function __construct(LoginRepository $loginRepo)
    {
        $this->loginRepository = $loginRepo;
    }

    /**
     * Display a listing of the Login.
     * GET|HEAD /logins
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->loginRepository->pushCriteria(new RequestCriteria($request));
        $this->loginRepository->pushCriteria(new LimitOffsetCriteria($request));
        $logins = $this->loginRepository->all();

        return $this->sendResponse($logins->toArray(), 'Logins retrieved successfully');
    }

    /**
     * Store a newly created Login in storage.
     * POST /logins
     *
     * @param CreateLoginAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLoginAPIRequest $request)
    {
        $input = $request->all();

        $logins = $this->loginRepository->create($input);

        return $this->sendResponse($logins->toArray(), 'Login saved successfully');
    }

    /**
     * Display the specified Login.
     * GET|HEAD /logins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Login $login */
        $login = $this->loginRepository->findWithoutFail($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        return $this->sendResponse($login->toArray(), 'Login retrieved successfully');
    }

    /**
     * Update the specified Login in storage.
     * PUT/PATCH /logins/{id}
     *
     * @param  int $id
     * @param UpdateLoginAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLoginAPIRequest $request)
    {
        $input = $request->all();

        /** @var Login $login */
        $login = $this->loginRepository->findWithoutFail($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        $login = $this->loginRepository->update($input, $id);

        return $this->sendResponse($login->toArray(), 'Login updated successfully');
    }

    /**
     * Remove the specified Login from storage.
     * DELETE /logins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Login $login */
        $login = $this->loginRepository->findWithoutFail($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        $login->delete();

        return $this->sendResponse($id, 'Login deleted successfully');
    }
}
