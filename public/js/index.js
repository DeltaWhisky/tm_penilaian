$(document).ready(function(){
    $(".full-header").sticky({ topSpacing: 0 });
    $(".full-header-mobile").sticky({ topSpacing: 0 });

    $(".main-menu-mobile").hide();	
    $(".menu-mobile .toggle-button a").click(function(){
        $(".main-menu-mobile").slideToggle();
    });
});